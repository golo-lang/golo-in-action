module kebab

# How to run it:
# golo golo --files kebab.golo

function main = |args| {

  # these closures return a boolean value
  let noPeppers = |item| -> item isnt "🌶️"
  let noOnions  = |item| -> item isnt "🧅"
  let noFries   = |item| -> item isnt "🍟"

  # these closure return a string value
  let pick = |item| -> "piece of " + item

  let ingredients = list["🍅", "🧅", "🥗", "🍖", "🌶", "🍟"]

  # I can compose my own kebab
  let myFavouriteRecipe =
    ingredients             # ["🍅", "🧅", "🥗", "🍖", "🌶", "🍟"]
      : filter(noFries)     # ["🍅", "🧅", "🥗", "🍖", "🌶"]
      : filter(noOnions)    # ["🍅", "🥗", "🍖", "🌶"]
      : filter(noPeppers)   # ["🍅", "🥗", "🍖"]
      : map(pick)           # ["piece of 🍅", "piece of 🥗", "piece of 🍖"]

  println(myFavouriteRecipe)

  # Deliver the kebab
  let mixIngredients = |accItem, nextItem| -> accItem + nextItem + " "

  let kebab = myFavouriteRecipe: reduce("🥙 with ", mixIngredients)

  println(kebab)
  # 🥙 with piece of 🍅 piece of 🥗 piece of 🍖 piece of 🌶
}
