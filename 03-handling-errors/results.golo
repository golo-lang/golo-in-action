module playWithResults

import gololang.Errors

@result
function toInt = |value| {
  return java.lang.Integer.parseInt(value)
}

function main = |args| {
  # destructuring
  let error, value = toInt("fourty-two")
  if (error oftype NumberFormatException.class) {
    println(error)
  } else {
    println(value)
  }

  toInt("42"): either(
    recover= |error| -> println(error),
    mapping= |value| -> println(value)
  )


}
