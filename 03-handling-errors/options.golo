module playWithOptions

import gololang.Errors

@option
function animal = |name| {
  let animals = map[
    ["panda", "🐼"],
    ["tiger", "🐯"],
    ["bear", "🐻"],
    ["lion", "🦁"]
  ]
  return animals: get(name)
}

function main = |args| {

  println(animal("panda"))
  println(animal("cow"))

  let panda = animal("panda"): map(|emoji| -> emoji): orElseGet(-> "💥")
  let cow = animal("cow"): map(|emoji| -> emoji): orElseGet(-> "💥")

  println(panda)
  println(cow)

  animal("panda"): either(
    default= -> println("💥"),
    mapping= |value| -> println(value)
  )

  animal("cow"): either(
    default= -> println("💥"),
    mapping= |value| -> println(value)
  )

}
