module playWithUnions

#import gololang.Errors

union HttpStatus = {
	Informational = { value }
	Successful = { value }
  Redirect = { value }
  ClientError = { value }
  ServerError = { value }
}

# HTTP response status codes indicate whether a specific HTTP request has been successfully completed. Responses are grouped in five classes:
#
# Informational responses (100–199)
# Successful responses (200–299)
# Redirects (300–399)
# Client errors (400–499)
# Server errors (500–599)

function main = |args| {

  let responseStatus = HttpStatus.Successful(201)

  if responseStatus: isSuccessful() {
    println(responseStatus: value())
  }

  let check_status_code = |status| -> match {
    when status: isClientError() then "client error: " + status: value()
    when status: isServerError() then "server error: " + status: value()
    when status: isRedirect() then "redirection: " + status: value()
    when status: isSuccessful() then "successful: " + status: value()
    when status: isInformational() then "informational: " + status: value()
    otherwise "🤔"
  }

  println(check_status_code(responseStatus))


}




