# golo golo --classpath libs/*.jar --files mqtt.module.golo main.golo
module main

import io.vertx.core.Vertx
import io.vertx.mqtt.MqttServer
import io.vertx.mqtt.MqttServerOptions

import gololang.Errors
import mqttModule

----
This example is not complete, but you can run it
to test it
----
function main = |args| {

  let vertx = Vertx.vertx()
  let mqtt_options = MqttServerOptions(): port(1883)
  let mqtt_server = MqttServer.create(vertx, mqtt_options)

  mqtt_server: endpointHandler(|endpoint| {

    mqtt_server: updateClients(endpoint)

    endpoint: subscribeHandler(|subscriptionRequest| {
      mqtt_server: updateSubscriptions(endpoint: clientIdentifier(), subscriptionRequest)
    })

    endpoint: publishHandler(|message| {
      println("message on topic: " + message: topicName())
      println("message content: " + message: payload())
      # at every message
      mqtt_server: mqttClients(): each(|identifier, client| {
        # check for each client if a subscription exists
        mqtt_server: mqttSubscriptions(): getOptional(identifier + "-" + message: topicName())
          : either(
              default= -> println("🖐️ no subscription for this client"),
              mapping= |subscription| ->
                client: simplePublish(
                  message: topicName(),
                  message: payload()
                )
            )
      })
    })

  })

  mqtt_server: listen()
  println("😄 listening on " + mqtt_server: actualPort())

}
