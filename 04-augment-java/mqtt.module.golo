module mqttModule

import io.netty.handler.codec.mqtt.MqttQoS
import io.vertx.core.buffer.Buffer
import io.vertx.mqtt.MqttEndpoint
import io.vertx.core.Vertx
import io.vertx.mqtt.MqttServer
import io.vertx.mqtt.MqttServerOptions

import gololang.Errors

# Module-level references are only visible from their module, 
# although a function may provide accessors to them.
let mqttClients = map[]
let mqttSubscriptions = map[]

augment java.util.Map {
  @option
  function getOptional = |this, key| -> this: get(key)
}

augment io.vertx.mqtt.MqttServer {
  
  function updateClients = |this, endpoint| {
    endpoint: accept(false)
    # update clients connection
    mqttClients: put(endpoint: clientIdentifier(), endpoint)
    println("🤖 connected client: " + endpoint: clientIdentifier())
  }

  function updateSubscriptions = |this, clientIdentifier, subscriptionRequest| {
    # update clients subscriptions
    subscriptionRequest: topicSubscriptions(): each(|subscription| {
      mqttSubscriptions: put(
        clientIdentifier + "-" + subscription: topicName(),
        subscription
      )
    })
    println("😊 new subscriptions(s): " + subscriptionRequest: topicSubscriptions(): head())
  }

  function mqttClients = |this| -> mqttClients

  function mqttSubscriptions = |this| -> mqttSubscriptions

}

augment io.vertx.mqtt.MqttEndpoint {
  function simplePublish = |this, topic, payload| {
    this: publish(
      topic,
      Buffer.buffer(payload: toString()),
      MqttQoS.AT_LEAST_ONCE(),
      false,
      false
    )
  }
}
