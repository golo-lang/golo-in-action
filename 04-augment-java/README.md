Run the example:

> start the MQTT Broker
```bash
golo golo --classpath libs/*.jar --files mqtt.module.golo main.golo
```

You can test it with the Node.js MQTT clients in this directory: `/mqtt-client-js`
