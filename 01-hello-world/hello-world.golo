module helloWorld

# How to run it:
# golo golo --files hello-world.golo

struct Person = { name }

function main = |args| {
  let bob = ImmutablePerson("Bob")
  let message = |person| -> "👋 Hello World 🌍\nI'm " + bob: name()
  println(message(bob))
}
